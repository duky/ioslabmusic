//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by markmota on 11/9/18.
//  Copyright © 2018 markmota. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        let session = Session.sharedInstance
        session.token = nil
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testCorrectLogin(){
        XCTAssert(User.login(userName: "iOSLab", password: "verysecurepassword"))
        
    }
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "iOSLadddb", password: "verysecurepasdwdwdsword"))
    }
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "verysecurepassword")
        XCTAssertNotNil(session.token)
    }
    func testWrongSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLadddddb", password: "verysecurepasdddddsword")
        XCTAssertNil(session.token)
    }
    func testExpectedToken(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "verysecurepassword")
        XCTAssertEqual(session.token,"123456666","Token Should Match")
    }
    func testNotExpectedToken(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "verysecurepassword")
        XCTAssertNotEqual(session.token,"ffdd","Token Should Not Match")
    }
    func testErrorFetchSongs(){
        XCTAssertThrowsError(try User.fetchSongs())
    }
    func testExpectedFetchSongs(){
        Session.sharedInstance.saveSession()
        XCTAssertNoThrow(try User.fetchSongs())
    }
    func testErrorParseData(){
        let dataReturned: Data = Data()
        XCTAssertNil(Music.parseData(data: dataReturned))
    }
    func testExpectedParseData(){
        var dataReturned: Data = Data()
        let urlSession = URLSession(configuration: .default)
        let promise = expectation(description: "Data downloaded")
        guard let url = URL(string: "https://itunes.apple.com/search/media=music&identity=song&term=Beatles") else {return}
        let dataTask = urlSession.dataTask(with: url){ data, response, error in
            if let error = error {
                debugPrint("Error in dataTask: \(error.localizedDescription)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, (200 ... 299).contains(httpResponse.statusCode) else{
                debugPrint("Error in httpResponse, code out of range between 200 ... 299")
                return
                
            }
            guard let data = data else{return}
            dataReturned = data
            promise.fulfill()
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(Music.parseData(data: dataReturned))
    }
    func testExpectedSongsForm(){
        var songs: [Song]?
        let nsDictionaryTest = NSDictionary(dictionary: ["results" : [NSDictionary(dictionary: ["trackName" : "Track name","artistName":"Title of the song"])]])
        songs = Music.songsForm(jsonNSDictionary: nsDictionaryTest)
        XCTAssertNotNil(songs)
    }
    
    func testErrorSongsForm(){
        var songs: [Song]?
        let nsDictionaryTest = NSDictionary(dictionary: ["resultjs" : [NSDictionary(dictionary: ["trackName" : "Track name","artistName":"Title of the song"])]])
        songs = Music.songsForm(jsonNSDictionary: nsDictionaryTest)
        XCTAssertNil(songs)
    }
    func testErrorSongsFormKeys(){
        var songs: [Song]?
        let nsDictionaryTest = NSDictionary(dictionary: ["results" : [NSDictionary(dictionary: ["traccckName" : "Track name","artccistName":"Title of the song"])]])
        songs = Music.songsForm(jsonNSDictionary: nsDictionaryTest)
        XCTAssertNil(songs)
    }
    
    func testMusicSongs() {
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fetchSongs(songName: "Letitbe", onSuccess: { (songs) in
            resultSongs = songs
            promise.fulfill()
        })
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
    
    func testExpectedSongCrete(){
        let songTest: Song?
        let nsDictionaryTest = NSDictionary(dictionary: ["trackName" : "Track name","artistName":"Title of the song"])
        songTest = Song.create(dict: nsDictionaryTest)
        XCTAssertNotNil(songTest)
    }
    func testErrorSongCrete(){
        let songTest: Song?
        let nsDictionaryTest = NSDictionary(dictionary: ["tracName" : "Track name","artistNam":"Title of the song"])
        songTest = Song.create(dict: nsDictionaryTest)
        XCTAssertNil(songTest)
    }
    
}
