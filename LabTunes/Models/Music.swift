//
//  Music.swift
//  LabTunes
//
//  Created by markmota on 11/9/18.
//  Copyright © 2018 markmota. All rights reserved.
//

import Foundation
class Music {
    static var urlSession = URLSession(configuration: .default)
    
    
    static func fetchSongs(songName: String = "TheBeatles", onSuccess: @escaping ([Song]) -> Void){
        
        guard let songName = songName.addingPercentEncoding(withAllowedCharacters: []),  let url = URL(string: "https://itunes.apple.com/search/media=music&identity=song&term=\(songName))") else {return}
        let dataTask = urlSession.dataTask(with: url){ data, response, error in
            if let error = error {
                debugPrint("Error in dataTask: \(error.localizedDescription)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, (200 ... 299).contains(httpResponse.statusCode) else{
                debugPrint("Error in httpResponse, code out of range between 200 ... 299")
                return
                
            }
            guard let data = data, let json = parseData(data: data) else{return}
            
            if let songs = songsForm(jsonNSDictionary: json){
                onSuccess(songs)
            }else{
                debugPrint("Can't create songs array")
            }
        }
        dataTask.resume()  
    }
    static func parseData(data:Data) -> NSDictionary? {
        let json = try? JSONSerialization.jsonObject(with: data, options: []), jsonNSDictionary = json as? NSDictionary
        return jsonNSDictionary
    }
    static func songsForm(jsonNSDictionary: NSDictionary) -> [Song]? {
        guard let results = jsonNSDictionary["results"], let resultsNSDictonary = results as? [NSDictionary] else {return nil}
        var songs: [Song] = []
        for dataResults in resultsNSDictonary {
            if let song = Song.create(dict: dataResults) {
                songs.append(song)
            }
            else{
                debugPrint("Can't create Song instance, and for that I don't append any song to the array")
            }
        }
        return (songs.count > 0 ? songs : nil )
    }
}
