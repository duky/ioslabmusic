//
//  ViewController.swift
//  LabTunes
//
//  Created by markmota on 11/9/18.
//  Copyright © 2018 markmota. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func loginButtonWasTouchUpInside(_ sender: Any) {
        
        guard let userName = userTextField.text, let password = passwordTextField.text else {
            debugPrint("Loggin incorrect")
            return
        }
        if User.login(userName: userName, password: password){
            performSegue(withIdentifier: "logSuccess", sender: self)
        }
        else {
            debugPrint("Wrong credentials")
        }
    }
    
    


}

