//
//  MusicController.swift
//  LabTunes
//
//  Created by markmota on 11/10/18.
//  Copyright © 2018 markmota. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {
    var songs : [Song] = []
    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupSearchBar()
        downloadSongs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func downloadSongs(by song: String="Beatles"){
        Music.fetchSongs(songName: song) { (result) in
            self.songs = result
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
    }
    func setupSearchBar(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search song ..."
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MusicViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        downloadSongs(by: searchController.searchBar.text!)
    }
    
    
}
extension MusicViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSong", for: indexPath)
        cell.textLabel?.text = songs[indexPath.row].artist
        return cell
    }
}
